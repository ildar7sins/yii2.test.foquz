<?php

namespace app\exception;

use yii\web\HttpException;

class ModelValidationException extends HttpException
{
    /**
     * @param $message
     * @param int $code
     * @param $previous
     */
    public function __construct($message = null, int $code = 0, $previous = null)
    {
        parent::__construct(400, $message, $code, $previous);
    }
}
