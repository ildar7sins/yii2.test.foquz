<?php

namespace app\resources;

use app\models\Patient as PatientModel;
use yii\db\ActiveQuery;

class Patient extends PatientModel
{
    public function fields(): array
    {
        return [
            'id', 'name', 'phone', 'polyclinic',
            'treatment_id', 'status_id', 'form_disease_id',
            'updated', 'updated_by', 'diagnosis_date',
            'analysis_date', 'source_id',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPolyclinic(): ActiveQuery
    {
        return $this->hasOne(Polyclinics::class, ['id' => 'polyclinic_id']);
    }
}
