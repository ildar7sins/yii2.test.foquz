<?php

namespace app\resources\search;

use app\resources\Patient;
use app\exception\ModelValidationException;

/**
 * UserSearch represents the model behind the search form about `webvimark\modules\UserManagement\models\User`.
 */
class PatientSearch extends Patient
{
    /**
     * @var string
     */
    public $orderBy = 'id';

    /**
     * @var string
     */
    public $ascending = '0';

    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var int
     */
    public $limit = 20;

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['page', 'limit', 'orderBy', 'ascending'], 'safe']
            ]
        );
    }

    /**
     * @param $params
     * @return array
     * @throws ModelValidationException
     */
    public function search($params): array
    {
        $query = self::find();

        $query->with(["polyclinic"]);

        if (!($this->load($params, '') && $this->validate())) {
            throw new ModelValidationException();
        }

        $query
            ->andFilterWhere([
                'polyclinic_id' => $this->polyclinic_id,
                'status_id' => $this->status_id,
                'form_disease_id' => $this->form_disease_id,
                'treatment_id' => $this->treatment_id,
            ])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        $query->orderBy($this->orderBy . ((int)$this->ascending === 1 ? ' ASC' : ' DESC'));

        return [
            'count' => $query->count(),
            'data' => $query->offset(--$this->page * $this->limit)->limit($this->limit)->all(),
        ];
    }
}
