<?php

namespace app\resources;

use app\models\Polyclinics as PolyclinicsModel;

class Polyclinics extends PolyclinicsModel {

    public function fields(): array
    {
        return ['id', 'name'];
    }
}