<?php

namespace app\controllers\api\v1;

use app;
use app\actions\api\IndexAction;
use app\models\RestController;
use app\resources\Patient;
use app\resources\search\PatientSearch;
use yii\rest\CreateAction;

class PatientsController extends RestController
{
    /**
     * @var string
     */
    public $modelClass = Patient::class;

    public function actions(): array
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => $this->modelClass,
                'searchModel' => PatientSearch::class,
            ],
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->modelClass,
            ],
        ];
    }
}

