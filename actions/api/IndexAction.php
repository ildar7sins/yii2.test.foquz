<?php

namespace app\actions\api;

use Yii;
use yii\base\InvalidConfigException;
use yii\rest\Action;

/**
 * @author Ildar Gaskarov <ildar7sins@gmail.com>
 */
class IndexAction extends Action
{
    public $searchModel;

    /**
     * @return mixed
     * @throws InvalidConfigException
     */
    public function run()
    {
        $userSearch = new $this->searchModel();
        return $userSearch->search(Yii::$app->request->get());
    }
}
