<?php

namespace app\actions\api;

use app\exception\ModelValidationException;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;
use yii\helpers\Url;
use yii\rest\Action;
use yii\web\ServerErrorHttpException;

/**
 * @author Ildar Gaskarov <ildar7sins@gmail.com>
 */
class CreateAction extends Action
{
    /**
     * Creates a new model.
     * @return ActiveRecordInterface the model newly created
     * @throws ServerErrorHttpException if there is any error when creating the model
     */
    public function run()
    {
        $data = Yii::$app->query->getData();

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /* @var $model ActiveRecord */
        $model = new $this->modelClass();
        $response = Yii::$app->getResponse();

        $model->load($data, '');
        if ($model->validate() && $model->save()) {
            $response->setStatusCode(201);
            $model->refresh();

            return $model;
        }

        $response->setStatusCode(400);
        return $model->getErrors();
    }
}
